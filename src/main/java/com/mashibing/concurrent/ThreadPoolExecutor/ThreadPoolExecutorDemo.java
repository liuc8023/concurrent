package com.mashibing.concurrent.ThreadPoolExecutor;

import java.util.concurrent.*;

/**
 * 自定义线程池
 */
public class ThreadPoolExecutorDemo {
    public static void main(String[] args) {
        //自定义线程池，此处使用的是定义了7个参数
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                2,
                5,
                500,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy()
        );

        try{

            /**
             * 模拟10个用户来办理业务，每个用户就是一个来自外部的请求线程此处设置的拒绝策略为AbortPolicy(),
             * 当处理线程都还没处理完第一轮所有请求的情况下，10个请求线程超过了最大线程数加阻塞队列最大值数
             * 无法处理多出的请求，就会启动拒绝策略，这里会抛出异常
             */
            for(int i=1;i<=10;i++){
                //execute():向线程池提交任务，该方法没有返回值，不会返回执行结果，该方法需要传入Runnable类的实例。
                executor.execute(()->{
                    System.out.println(Thread.currentThread().getName()+"\t 办理业务");
                });
            }

            /**
             * submit()：向线程池提交任务，会返回future对象，通过future对象判断任务是否执行成功，并通过future对象的get()方法获取返回值。
             * 其中get()方法会阻塞当前线程直到任务完成，而get(long timeout,TimeUnit  unit)方法则会阻塞当前线程一段时间后立即返回，
             * 这时有可能任务没有执行完。
             */
            Future<Object> future = executor.submit(new Callable<Object>() {
                @Override
                public Object call() throws Exception {
                    System.out.println("哈哈");
                    return "success";
                }
            });
            try {
                System.out.println("future.get():"+future.get());
            } catch (InterruptedException e) {
                // 处理中断异常
                e.printStackTrace();
            } catch (ExecutionException e) {
                // 处理无法执行任务异常
                e.printStackTrace();
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            // 关闭线程池
            executor.shutdown();
        }

    }
}
