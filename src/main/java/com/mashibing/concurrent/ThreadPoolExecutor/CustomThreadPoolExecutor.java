package com.mashibing.concurrent.ThreadPoolExecutor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * @author liuc
 */
public class CustomThreadPoolExecutor {
    private static class SingletonHolder {
        private static final CustomThreadPoolExecutor INSTANCE = new CustomThreadPoolExecutor();
    }
    private CustomThreadPoolExecutor (){}
    public static final CustomThreadPoolExecutor getInstance() {
        return SingletonHolder.INSTANCE;
    }
    /**
     * 自定义线程池阻塞有界队列
     */
    private static LinkedBlockingQueue<Runnable> linkedBlockingQueue;
    /**
     * 队列当前大小
     */
    private static int currentQueueSize;
    /**
     * 队列容量
     */
    private static int queueCapacity;
    /**
     * 执行接口调用的线程池
     */
    private static ExecutorService executorService;

    /**
     * 初始化线程池
     * **/
    static {
        //默认-核心线程池大小
        int corePoolSize = 5;
        //默认-最大线程池大小
        int maximumPoolSize = 20;
        //默认-线程池中超过corePoolSize数目的空闲线程最大存活时间(秒)
        long keepAliveTime = 30;
        //默认-阻塞队列大小
        int blockingQueueSize = 60;

        try {
            /**
             * corePoolSize 核心线程池大小
             * maximumPoolSize 最大线程池大小
             * keepAliveTime 线程池中超过corePoolSize数目的空闲线程最大存活时间
             * TimeUnit.SECONDS  keepAliveTime时间单位
             * new LinkedBlockingQueue<Runnable>():阻塞任务队列
             * new DmptServicesThreadFactory():threadFactory 新建线程工厂
             * new ThreadPoolExecutor.AbortPolicy():RejectedExecutionHandler 当提交任务数超过maxmumPoolSize+workQueue之和时，任务交给ThreadPoolExecutor.AbortPolicy()处理
             */
            linkedBlockingQueue = new LinkedBlockingQueue<Runnable>(blockingQueueSize);
            executorService = new ThreadPoolExecutor(
                    corePoolSize,
                    maximumPoolSize,
                    keepAliveTime,
                    TimeUnit.SECONDS,
                    linkedBlockingQueue,
                    new CustomThreadFactory(),
                    new ThreadPoolExecutor.AbortPolicy()
            );
        } catch (Exception e) {
            System.out.println("AsyncInvoke create ExecutorService error."+e.getMessage());
        }
    }

    public static Map<String, Object> concurrentInvokeServices(Map<String, Object> context){
        //并发调用返回结果汇总
        Map<String, Object> returnMap = new HashMap<String, Object>(8);
        try {
            List<Map<String,Object>> servicesNameAndInputInfo = (List<Map<String, Object>>)context.get("servicesNameAndInputInfo");
            int newTaskSize = servicesNameAndInputInfo.size();

            //当前任务数 > 队列空闲数量 时,向调用端返回错误map
            currentQueueSize = linkedBlockingQueue.size();
            System.out.println("queueCapacity:" + queueCapacity + ",currentQueueSize:" + currentQueueSize + ",newTaskSize:" + newTaskSize);
            if(newTaskSize > (queueCapacity - currentQueueSize) ) {
                System.out.println("任务数大于空闲队列大小!");
//                return ReturnMapUtil.getErrorMap("服务器前当前无空闲资源(线程已打满),请稍后再试!");
            }


            CountDownLatch countDownLatch = new CountDownLatch(servicesNameAndInputInfo.size());

            for (Map<String, Object> map : servicesNameAndInputInfo) {
                String serviceName = (String)map.get("serviceName");//被调接口
//                Map<String,Object> serviceInputMap =  (Map<String, Object>)map.get(SERVICE_INPUT_MAP);//被调接口入参MAP
//                String retMapName = (String)map.get(RET_MAP_NAME);//定义被调方返回map的名称，用于区分是哪个被调方
                try {
                    Future<Object> future = executorService.submit(new Callable<Object>() {
                        @Override
                        public Object call() throws Exception {
                            System.out.println(serviceName+":哈哈");
                            return serviceName+"：success";
                        }
                    });
                    returnMap.put(serviceName, future.get());
                }catch (Exception e) {
                    throw e;
                }finally {
                    // 任务执行完，计数器 -1
                    countDownLatch.countDown();
                }
            }
            // 阻塞等待线程池任务执行完
            countDownLatch.await();
            System.out.println("线程池任务全部执行完成!");
        } catch (Exception e) {

        }
        return returnMap;
    }

    public static void main(String[] args) {
        List<Map<String, Object>> servicesNameAndInputInfo = new ArrayList<Map<String, Object>>();
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> MethodMap1 = new HashMap<String, Object>();
        MethodMap1.put("serviceName", "queryCreditApplyMainInfo");
        servicesNameAndInputInfo.add(MethodMap1);
        Map<String, Object> MethodMap2 = new HashMap<String, Object>();
        MethodMap2.put("serviceName", "queryCreditApplyMain");
        servicesNameAndInputInfo.add(MethodMap2);
        map.put("servicesNameAndInputInfo", servicesNameAndInputInfo);
        CustomThreadPoolExecutor customerThreadPoolExecutor = CustomThreadPoolExecutor.getInstance();
        Map respMap = customerThreadPoolExecutor.concurrentInvokeServices(map);
        System.out.println(respMap.get("queryCreditApplyMainInfo"));
        System.out.println(respMap.get("queryCreditApplyMain"));
    }
}
