package com.mashibing.concurrent.RefTypeAndThreadLocal;

import java.lang.ref.SoftReference;
import java.util.concurrent.TimeUnit;

/**
 * 软引用
 */
public class T02_SoftReference {
    public static void main(String[] args) {
        SoftReference<byte[]> sr = new SoftReference<>(new byte[1024 * 1024 * 10]);
        System.out.println(sr.get());
        //垃圾回收
        System.gc();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(sr.get());
        //再分配一个数组，heap将装不下，这个时候系统会垃圾回收，先回收一次，如果不够，会把软引用踢掉
        byte[] b = new byte[1024 * 1024 * 12];
        System.out.println(sr.get());
    }
}
