package com.mashibing.concurrent.RefTypeAndThreadLocal;

/**
 *
 * @author liuc
 * @date 2022/06/19
 */
public class Person {
    public Person(String name) {
        this.name = name;
    }

    /**
     * 集名称
     *
     * @param name 名字
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 名字
     */
    private String name ="zhangsan";


}
