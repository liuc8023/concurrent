package com.mashibing.concurrent.RefTypeAndThreadLocal;

import java.io.IOException;

/**
 * 强引用：强引用是最传统的“引用”的定义，是指在程序代码之中普遍存在的引用赋值，即类似“Objectobj=new Object()”这种引用关系。
 * 无论任何情况下，只要强引用关系还存在，垃圾收集器就永远不会回收掉被引用的对象。
 */
public class T01_NormalReference {
    public static void main(String[] args) throws IOException {
        M m = new M();
        m = null;
        System.gc();//DisableExplicitGC
        System.out.println(m);
        System.in.read();//阻塞main线程，给垃圾回收线程时间执行
    }

}
