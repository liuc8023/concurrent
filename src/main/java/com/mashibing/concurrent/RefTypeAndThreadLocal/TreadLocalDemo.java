package com.mashibing.concurrent.RefTypeAndThreadLocal;

import java.util.concurrent.TimeUnit;

public class TreadLocalDemo {
    static ThreadLocal<Person> tl = new ThreadLocal<Person>();

    public static void main(String[] args) {
        new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            tl.set(new Person("lisi"));
            System.out.println(tl.get());
        },"线程A").start();

        new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(tl.get());
        },"线程B").start();
    }
}
