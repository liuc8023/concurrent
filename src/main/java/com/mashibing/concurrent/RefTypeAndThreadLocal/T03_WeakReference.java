package com.mashibing.concurrent.RefTypeAndThreadLocal;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

/**
 * 弱引用:只要gc发现弱引用对象就把它干掉
 */
public class T03_WeakReference {
    public static void main(String[] args) {
        WeakReference<M> wr = new WeakReference<>(new M());
        System.out.println(wr.get());
        System.gc();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(wr.get());
    }
}
