package com.concurrent.thread.vol;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicStampedReference;

class ABATest{
    // 普通的原子类，存在ABA问题
    AtomicInteger a1 = new AtomicInteger(10);
    // 带有时间戳的原子类，不存在ABA问题，第二个参数就是默认时间戳，这里指定为0
    AtomicStampedReference<Integer> a2 = new AtomicStampedReference<Integer>(10, 0);
}

/**
 * 测试CAS的ABA问题
 */
public class ABADemo {
    public static void main(String[] args) {
        ABATest abaTest = new ABATest();
        new Thread(()->{
            abaTest.a1.compareAndSet(10,11);
            abaTest.a1.compareAndSet(11,10);
        },"线程A").start();
        new Thread(()->{
            try {
                //睡0.2秒
                TimeUnit.MILLISECONDS.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("AtomicInteger原子操作："+abaTest.a1.compareAndSet(10,11));
        },"线程B").start();
        new Thread(()->{
            try {
                //睡0.5秒，保证线程D先执行
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int stamp = abaTest.a2.getStamp();
            abaTest.a2.compareAndSet(10,11,stamp,stamp+1);
            stamp = abaTest.a2.getStamp();
            abaTest.a2.compareAndSet(11,10,stamp,stamp+1);
        },"线程C").start();
        new Thread(()->{
            int stamp = abaTest.a2.getStamp();
            try {
                //睡一秒，给线程C时间做ABA操作
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("AtomicStampedReference原子操作:" + abaTest.a2.compareAndSet(10, 11, stamp, stamp + 1));
        },"线程D").start();
    }
}
