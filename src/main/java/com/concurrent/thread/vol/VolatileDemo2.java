package com.concurrent.thread.vol;

class Test2{
    volatile int number = 0;

    public void add(){
        number++;
    }
}

/**
 * 测试volatile不保证原子性
 */
public class VolatileDemo2 {
    public static void main(String[] args) {
        Test2 test2 = new Test2();
        for (int i = 0; i < 20; i++) {
            new Thread(()->{
                for (int j = 0; j < 1000; j++) {
                    test2.add();
                }
            }).start();
        }
        //需要等待上面20个线程都完全计算完成后，再加上main线程取得最终的结果值看是多少
        while(Thread.activeCount()>2){
            // 礼让，如果检查到线程存活数为2，则告诉CPU不继续执行下面的逻辑
            Thread.yield();
        }
        System.out.println(Thread.currentThread().getName()+"\t finally number value:"+test2.number);
    }
}
