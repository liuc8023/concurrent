package com.concurrent.thread.vol;

import java.util.concurrent.TimeUnit;

class Test {
    volatile int number = 0;

    public void updateNumber(){
        number = 60;
    }
}

/**
 * 1 验证volatile的可见性
 * 1.1 假如int number = 0; number 变量之前根本没有添加volatile关键字修饰
 *
 */
public class VolatileDemo1{
    public static void main(String[] args) {
        Test t = new Test();
        new Thread(()->{
            System.out.println(Thread.currentThread().getName()+"\t，未做修改之前获取到的数据："+t.number);
            try {
                //暂停线程3秒钟
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            t.updateNumber();
            System.out.println(Thread.currentThread().getName()+"\t，三秒之后修改number值，再次获取到的数据："+t.number);
        },"线程AAA").start();

        while(t.number == 0){
            //main线程就一直在这里等待循环，直到number值不再等于零。
        }
        System.out.println(Thread.currentThread().getName()+"\t mission is over,main get number value:"+t.number);
    }
}
