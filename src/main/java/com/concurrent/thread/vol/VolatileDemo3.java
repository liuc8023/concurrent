package com.concurrent.thread.vol;

import java.util.concurrent.atomic.AtomicInteger;

class Test3{
    volatile int number = 0;

    /**
     * 1、为了解决原子性的问题，可以使用synchronized关键字修饰
     */
    public synchronized void add(){
        number++;
    }

    /**
     * 2、使用JUC包下的AtomicInteger
     */
    AtomicInteger atomicInteger = new AtomicInteger();
    public void atomicAdd(){
        atomicInteger.getAndIncrement();
    }
}

/**
 * 如何解决原子性？
 */
public class VolatileDemo3 {
    public static void main(String[] args) {
        Test3 test3 = new Test3();
        for (int i = 0; i < 20; i++) {
            new Thread(()->{
                for (int j = 0; j < 1000; j++) {
                    test3.add();
                    test3.atomicAdd();
                }
            }).start();
        }
        //需要等待上面20个线程都完全计算完成后，再加上main线程取得最终的结果值看是多少
        while(Thread.activeCount()>2){
            // 礼让，如果检查到线程存活数为2，则告诉CPU不继续执行下面的逻辑
            Thread.yield();
        }
        System.out.println(Thread.currentThread().getName()+"\t int type,finally number value:"+test3.number);
        System.out.println(Thread.currentThread().getName()+"\t AtomicInteger type,finally number value:"+test3.atomicInteger);
    }
}
