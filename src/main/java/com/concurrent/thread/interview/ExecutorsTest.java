package com.concurrent.thread.interview;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 使用JDK自带的Executors来创建线程池对象
 * @author liuc
 */
public class ExecutorsTest implements Runnable{
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+" thread run...");
    }

    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(10);
        for (int i = 0; i < 10; i++) {
            executor.execute(new ExecutorsTest());
        }
        executor.shutdown();
    }
}
