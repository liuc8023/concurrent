package com.concurrent.thread.interview;

/**
 * @author liuc
 * 实现Runnable接口的方式创建线程
 */
public class ImplRunnableTest implements Runnable{
    @Override
    public void run() {
        System.out.println("hello i'am new thread...");
    }

    public static void main(String[] args) {
        new Thread(new ImplRunnableTest()).start();
    }
}
