package com.concurrent.thread.interview;

/**
 * @author liuc
 * 继承Thread类的方式创建线程
 */
public class ExtendsThreadTest extends Thread{
    @Override
    public void run() {
        System.out.println("hello i'am new thread...");
    }

    public static void main(String[] args) {
        new ExtendsThreadTest().start();
    }
}
