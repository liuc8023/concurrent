package com.concurrent.thread.interview;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author liuc
 * 实现Callable接口，并结合Future实现
 */
public class ImplCallableFuture implements Callable<Object> {
    @Override
    public Object call() throws Exception {
        return "hello callable";
    }

    public static void main(String[] args) {
        FutureTask<Object> task = new FutureTask<>(new ImplCallableFuture());
        new Thread(task).start();
        try {
            String message = (String) task.get();
            System.out.println(message);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
