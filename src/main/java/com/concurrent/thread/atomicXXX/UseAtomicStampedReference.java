package com.concurrent.thread.atomicXXX;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicStampedReference;

public class UseAtomicStampedReference {

    public static void main(String[] args) throws Exception {
        //使用AtomicStampedReference解决ABA问题
        System.out.println("==========Use AtomicStampedReference解决ABA问题：");
        AtomicStampedReference<String> reference = new AtomicStampedReference<>("A",0);
        new Thread(() -> {
            //获取期望值
            String expect = reference.getReference();
            //打印期望值
            System.out.println(Thread.currentThread().getName() + "---- expect: " + expect);
            try {
                //干点别的事情
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //打印实际值
            reference.set("X",reference.getStamp()+1);
            System.out.println(Thread.currentThread().getName() + "---- actual: " + reference.getReference());
            //进行CAS操作
            boolean result = reference.compareAndSet("A", "X",reference.getStamp(),reference.getStamp()+1);
            //打印操作结果
            System.out.println(Thread.currentThread().getName() + "---- result: " + result + " ==》 final reference = " + reference.getReference());
        }).start();

        new Thread(() -> {
            try {
                //干点别的事情
                TimeUnit.MICROSECONDS.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //进行ABA操作
            System.out.print(Thread.currentThread().getName() + "---- change: " + reference.getReference());
            reference.set("B",reference.getStamp()+1);
            reference.compareAndSet("A", "B",reference.getStamp(),reference.getStamp()+1);
            System.out.print(" -- > B");
            reference.compareAndSet("B", "A",reference.getStamp(),reference.getStamp()+1);
            System.out.println(" -- > A");
        }).start();
    }

}
