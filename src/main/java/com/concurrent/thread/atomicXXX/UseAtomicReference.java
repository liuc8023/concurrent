package com.concurrent.thread.atomicXXX;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 演示引用类型的原子操作类
 * 后面可能出现的情况大家应该能猜到了，这就是线程不安全的了，那如何保证线程安全呢？
 * 我们可以使用AtomicStampedReference或者AtomicMarkableReference解决ABA问题
 */
public class UseAtomicReference {

    public static void main(String[] args) throws Exception {
        //ABA问题
        System.out.println("==========ABA问题：");
        AtomicReference<String> reference = new AtomicReference<>("A");
        new Thread(() -> {
            //获取期望值，现在内存中的值
            String expect = reference.get();
            //打印期望值
            System.out.println(Thread.currentThread().getName() + "---- expect: " + expect);
            try {
                //干点别的事情，线程A睡上1秒钟，保证线程B能够有足够的时间进行ABA操作
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //打印实际值
            System.out.println(Thread.currentThread().getName() + "---- actual: " + reference.get());
            //进行CAS操作，如果内存中的值是A，那么就将A改成X
            boolean result = reference.compareAndSet("A", "X");
            //打印操作结果
            System.out.println(Thread.currentThread().getName() + "---- result: " + result + " ==》 final reference = " + reference.get());
        },"线程A").start();

        new Thread(() -> {
            try {
                //干点别的事情
                TimeUnit.MICROSECONDS.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //进行ABA操作
            System.out.print(Thread.currentThread().getName() + "---- change: " + reference.get());
            //如果内存中的值是A，那么就将A改为B
            reference.compareAndSet("A", "B");
            System.out.print(" -- > B");
            //如果内存中的值是B，那么就将B改为A
            reference.compareAndSet("B", "A");
            System.out.println(" -- > A");
        },"线程B").start();
    }

}
