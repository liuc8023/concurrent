package com.concurrent.thread.atomicXXX;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicMarkableReference;

public class UseAtomicMarkableReference {
    static AtomicMarkableReference<String> reference = new AtomicMarkableReference<>("A",Boolean.FALSE);
    public static void main(String[] args) throws Exception {
        //使用AtomicMarkableReference解决ABA问题
        System.out.println("==========Use AtomicMarkableReference解决ABA问题：");

        new Thread(() -> {
            //获取期望值，现在内存中的值
            String expect = reference.getReference();
            //打印期望值
            System.out.println(Thread.currentThread().getName() + "---- expect: " + expect);
            try {
                //干点别的事情，线程A睡上1秒钟，保证线程B能够有足够的时间进行ABA操作
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //打印实际值
            reference.set("X",Boolean.TRUE);
            System.out.println(Thread.currentThread().getName() + "---- actual: " + reference.getReference());
            //进行CAS操作
            boolean result = reference.compareAndSet("A", "X",reference.isMarked(),Boolean.TRUE);
            //打印操作结果
            System.out.println(Thread.currentThread().getName() + "---- result: " + result + " ==》 final reference = " + reference.getReference());
        },"线程A").start();

        new Thread(() -> {
            try {
                //干点别的事情
                TimeUnit.MICROSECONDS.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //进行ABA操作
            System.out.print(Thread.currentThread().getName() + "---- change: " + reference.getReference());
            reference.set("B",Boolean.TRUE);
            reference.compareAndSet("A", "B",reference.isMarked(),Boolean.TRUE);
            System.out.print(" -- > B");
            reference.compareAndSet("B", "A",reference.isMarked(),Boolean.TRUE);
            System.out.println(" -- > A");
        },"线程B").start();
    }
}
