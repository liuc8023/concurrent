package com.concurrent.juc;

/**
 * @author liuc
 * @version V1.0
 * @date 2022/3/6 16:28
 * @since JDK1.8
 */
@FunctionalInterface
interface Foo {
//    public void sayHello();

    public int add(int x,int y);

    default int div(int x,int y){
        return x/y;
    }
    default int div2(int x,int y){
        return x/y;
    }

    public static int mv(int x,int y){
        return x*y;
    }

    public static int mv2(int x,int y){
        return x*y;
    }
}

public class LambdaExpressDemo {
    public static void main(String[] args) {
        /*Foo foo = new Foo() {
            @Override
            public void sayHello() {
                System.out.println("***** hello");
            }
        };
        foo.sayHello();*/
        /*Foo foo = () -> {System.out.println("***** hello lambda Express");};
        foo.sayHello();*/
        Foo foo = (x,y)->{
            System.out.println("come in here");
            return x+y;
        };
        System.out.println(foo.add(3,4));
        System.out.println(foo.div(10,5));
        System.out.println(foo.div2(9,3));
        System.out.println(Foo.mv(3,6));
        System.out.println(Foo.mv2(4,7));
    }
}
