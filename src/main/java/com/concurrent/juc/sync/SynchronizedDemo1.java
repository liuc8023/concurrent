package com.concurrent.juc.sync;

import java.util.concurrent.TimeUnit;

class PhoneTest1{
    private int number = 0;
    public synchronized void increment(){
        System.out.println(Thread.currentThread().getName()+"开始执行时间："+System.currentTimeMillis());
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"\t" + (++number));
        System.out.println(Thread.currentThread().getName()+"执行结束时间："+System.currentTimeMillis());
    }
}

/**
 * 测试两个线程同时访问同一个对象的同一个同步方法
 * 这种情况是经典的对象锁中的方法锁，两个线程争夺同一个对象锁，所以会相互等待，是线程安全的。
 *
 * 两个线程同时访问同一个对象的同步方法，是线程安全的。
 */
public class SynchronizedDemo1 {
    public static void main(String[] args) throws InterruptedException {
        PhoneTest1 phoneTest1 = new PhoneTest1();
        new Thread(()->{phoneTest1.increment();},"A").start();
        new Thread(()->{phoneTest1.increment();},"B").start();
    }
}
