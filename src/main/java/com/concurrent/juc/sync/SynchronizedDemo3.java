package com.concurrent.juc.sync;

import java.util.concurrent.TimeUnit;

class PhoneTest3{
    private static int number = 0;
    public static synchronized void increment(){
        System.out.println(Thread.currentThread().getName()+"开始执行时间："+System.currentTimeMillis());
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"\t" + (++number));
        System.out.println(Thread.currentThread().getName()+"执行结束时间："+System.currentTimeMillis());
    }

    public static synchronized void increment2(){
        System.out.println(Thread.currentThread().getName()+"开始执行时间："+System.currentTimeMillis());
        System.out.println(Thread.currentThread().getName()+"\t" + (++number));
        System.out.println(Thread.currentThread().getName()+"执行结束时间："+System.currentTimeMillis());
    }

}

/**
 * 测试两个线程同时访问（一个或两个）对象的静态同步方法
 */
public class SynchronizedDemo3 {
    public static void main(String[] args) throws InterruptedException {
        PhoneTest3 phone1 = new PhoneTest3();
        PhoneTest3 phone2 = new PhoneTest3();
        new Thread(()->{phone1.increment();},"A").start();
        TimeUnit.SECONDS.sleep(1);
        new Thread(()->{phone2.increment2();},"B").start();
    }
}
