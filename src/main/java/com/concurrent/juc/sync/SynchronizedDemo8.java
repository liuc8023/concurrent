package com.concurrent.juc.sync;

import java.util.concurrent.TimeUnit;

class PhoneTest8{
    private int number = 0;
    public synchronized void increment(){
        System.out.println(Thread.currentThread().getName()+"开始执行时间："+System.currentTimeMillis());
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"\t" + (++number));
        System.out.println(Thread.currentThread().getName()+"执行结束时间："+System.currentTimeMillis());
        //同步方法中，当抛出异常时，JVM会自动释放锁，不需要手动释放，其他线程即可获取到该锁
        System.out.println("线程名：" + Thread.currentThread().getName() + "，抛出异常，释放锁");
        throw new RuntimeException();
    }

    public synchronized void increment2(){
        System.out.println(Thread.currentThread().getName()+"开始执行时间："+System.currentTimeMillis());
        System.out.println(Thread.currentThread().getName()+"\t" + (++number));
        System.out.println(Thread.currentThread().getName()+"执行结束时间："+System.currentTimeMillis());
    }
}

/**
 * 测试同步方法抛出异常后，JVM会自动释放锁的情况
 */
public class SynchronizedDemo8 {
    public static void main(String[] args) {
        PhoneTest8 phone1 = new PhoneTest8();
        new Thread(()->{phone1.increment();},"A").start();
        new Thread(()->{phone1.increment2();},"B").start();
    }
}
