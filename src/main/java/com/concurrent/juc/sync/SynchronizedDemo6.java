package com.concurrent.juc.sync;

import java.util.concurrent.TimeUnit;

class PhoneTest6{
    private int number = 0;

    public synchronized  void increment(){
        System.out.println(Thread.currentThread().getName()+"开始调用同步方法，运行开始执行时间："+System.currentTimeMillis());
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"\t" + (++number));
        System.out.println(Thread.currentThread().getName()+"同步方法执行结束时间："+System.currentTimeMillis());
        System.out.println(Thread.currentThread().getName()+"开始调用普通方法，运行开始执行时间："+System.currentTimeMillis());
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        increment2();
        System.out.println(Thread.currentThread().getName()+"执行结束时间："+System.currentTimeMillis());
    }

    /**
     * 普通方法
     */
    public void increment2(){
        System.out.println(Thread.currentThread().getName()+"开始调用普通方法，运行开始执行时间："+System.currentTimeMillis());
        System.out.println(Thread.currentThread().getName()+"\t" + (++number));
        System.out.println(Thread.currentThread().getName()+"普通方法执行结束时间："+System.currentTimeMillis());
    }
}

/**
 * 测试两个线程访问同一个对象中的同步方法，同步方法又调用一个非同步方法
 */
public class SynchronizedDemo6 {
    public static void main(String[] args) {
        PhoneTest6 phone1 = new PhoneTest6();
        new Thread(()->{phone1.increment();},"A").start();
        new Thread(()->{phone1.increment();},"B").start();
        new Thread(()->{phone1.increment2();},"C").start();
    }
}
