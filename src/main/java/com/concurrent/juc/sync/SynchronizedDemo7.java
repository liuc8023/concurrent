package com.concurrent.juc.sync;

import java.util.concurrent.TimeUnit;

class PhoneTest7{
    private int number = 0;
    public synchronized void increment(){
        System.out.println(Thread.currentThread().getName()+"开始执行时间："+System.currentTimeMillis());
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"\t" + (++number));
        System.out.println(Thread.currentThread().getName()+"执行结束时间："+System.currentTimeMillis());
    }

    public synchronized void increment2(){
        System.out.println(Thread.currentThread().getName()+"开始执行时间："+System.currentTimeMillis());
        System.out.println(Thread.currentThread().getName()+"\t" + (++number));
        System.out.println(Thread.currentThread().getName()+"执行结束时间："+System.currentTimeMillis());
    }
}

/**
 * 测试两个线程同时访问同一个对象的不同的同步方法
 * 一个对象里面如果有多个synchronized方法，某一个时刻内，只要一个线程去调用其中的一个synchronized方法了，
 * 其他的线程都只能等待，换句话说，某一个时刻内，只能有唯一一个线程去访问这些synchronized方法，锁的是该锁所在的类即
 * 当前对象this，被锁定后，其他的线程都不能进入到当前对象的其他的synchronized方法。
 */
public class SynchronizedDemo7 {
    public static void main(String[] args) throws InterruptedException {
        PhoneTest7 phoneTest1 = new PhoneTest7();
        new Thread(()->{phoneTest1.increment();},"A").start();
        new Thread(()->{phoneTest1.increment2();},"B").start();
        // new Thread(()->{phoneTest1.increment();},"B").start();
    }
}
