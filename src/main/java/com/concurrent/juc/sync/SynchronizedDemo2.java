package com.concurrent.juc.sync;

import java.util.concurrent.TimeUnit;

class PhoneTest2{
    private int number = 0;
    public synchronized void increment(){
        System.out.println(Thread.currentThread().getName()+"开始执行时间："+System.currentTimeMillis());
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"\t" + (++number));
        System.out.println(Thread.currentThread().getName()+"执行结束时间："+System.currentTimeMillis());
    }

    public synchronized void increment2(){
        System.out.println(Thread.currentThread().getName()+"开始执行时间："+System.currentTimeMillis());
        System.out.println(Thread.currentThread().getName()+"\t" + (++number));
        System.out.println(Thread.currentThread().getName()+"执行结束时间："+System.currentTimeMillis());
    }
}

/**
 * 测试两个线程同时访问两个对象的同步方法
 * 这种场景就是对象锁失效的场景，原因出在访问的是两个对象的同步方法，那么这两个线程分别持有的两个对象的锁，
 * 所以是互相不会受限的。加锁的目的是为了让多个线程竞争同一把锁，而这种情况多个线程之间不再竞争同一把锁，
 * 而是分别持有一把锁，所以我们的结论是：
 *
 * 两个线程同时访问两个对象的同步方法，是线程不安全的。
 */
public class SynchronizedDemo2 {
    public static void main(String[] args) throws InterruptedException {
        PhoneTest2 phone1 = new PhoneTest2();
        PhoneTest2 phone2 = new PhoneTest2();
        new Thread(()->{phone1.increment();},"A").start();
        TimeUnit.SECONDS.sleep(1);
        new Thread(()->{phone2.increment2();},"B").start();
    }
}
