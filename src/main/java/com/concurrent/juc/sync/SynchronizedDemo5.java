package com.concurrent.juc.sync;

import java.util.concurrent.TimeUnit;

class PhoneTest5 {
    private int number = 0;

    /**
     * 同步方法
     */
    public synchronized void increment(){
        System.out.println(Thread.currentThread().getName()+"开始执行时间："+System.currentTimeMillis());
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"\t" + (++number));
        System.out.println(Thread.currentThread().getName()+"执行结束时间："+System.currentTimeMillis());
    }

    /**
     * 普通方法
     */
    public void increment2(){
        System.out.println(Thread.currentThread().getName()+"开始执行时间："+System.currentTimeMillis());
        System.out.println(Thread.currentThread().getName()+"\t" + (++number));
        System.out.println(Thread.currentThread().getName()+"执行结束时间："+System.currentTimeMillis());
    }
}

/**
 * 测试两个线程其中一个访问同步方法，另一个访问非同步方法
 */
public class SynchronizedDemo5 {
    public static void main(String[] args) {
        PhoneTest5 phone1 = new PhoneTest5();
        PhoneTest5 phone2 = new PhoneTest5();
        new Thread(()->{phone1.increment();},"A").start();
//        new Thread(()->{phone1.increment2();},"B").start();
        new Thread(()->{phone2.increment2();},"B").start();
    }
}
