package cn.itcast.methods;

import lombok.extern.log4j.Log4j2;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 完成文件输出的守护线程任务
 */
@Log4j2
public class ThreadDaemonIo implements Runnable{

    @Override
    public void run() {
        try {
            //守护线程阻塞1秒后运行
            Thread.sleep(1000);
            File f=new File("daemon.txt");
            FileOutputStream os = new FileOutputStream(f,true);
            os.write("daemon".getBytes());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ThreadDaemonIo r = new ThreadDaemonIo();
        Thread t = new Thread(r);
        //设置守护线程
        t.setDaemon(true);
        t.start();
    }
}
