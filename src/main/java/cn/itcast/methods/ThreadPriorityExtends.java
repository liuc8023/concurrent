package cn.itcast.methods;

import lombok.extern.log4j.Log4j2;

/**
 * 测试线程优先级的继承性
 */
@Log4j2
public class ThreadPriorityExtends implements Runnable{
    @Override
    public void run() {
        ThreadPriorityExtends1 r1 = new ThreadPriorityExtends1();
        Thread t1 = new Thread(r1,"B");
        log.info(t1.getName()+"线程的优先级是:"+Thread.currentThread().getPriority());
    }
}

class ThreadPriorityExtends1 extends ThreadPriorityExtends{

}

@Log4j2
class Test{
    public static void main(String[] args) {
        log.info(Thread.currentThread().getName()+":"+Thread.currentThread().getPriority());
        ThreadPriorityExtends r = new ThreadPriorityExtends();
        Thread t = new Thread(r,"A");
        log.info(t.getName()+"修改线程优先级之前的优先级是:"+t.getPriority());
        t.setPriority(Thread.MAX_PRIORITY);
        log.info(t.getName()+"修改线程优先级之后的优先级是:"+t.getPriority());
        t.start();
    }
}
