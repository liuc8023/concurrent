package cn.itcast.methods;

import lombok.extern.log4j.Log4j2;

/**
 * 测试让main线程终止
 */
@Log4j2
public class ThreadInterrupted2{

    public static void main(String[] args) {
        log.info("给线程{}打终止标志",Thread.currentThread().getName());
        Thread.currentThread().interrupt();
        log.info("线程{}是否已经停止 1？={}",Thread.currentThread().getName(),Thread.interrupted());
        log.info("线程{}是否已经停止 2？={}",Thread.currentThread().getName(),Thread.interrupted());
        log.info("end!");
    }
}