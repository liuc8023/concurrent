package cn.itcast;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Test1 {

    public static void main(String[] args) {
        //创建线程对象
        Thread t = new Thread("t1") {
            @Override
            public void run() {
                //要执行的任务
                log.info("running");
            }
        };
        //启动线程
        t.start();
        log.info("running");
    }
}
