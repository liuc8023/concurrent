package cn.itcast.sync;

import lombok.extern.log4j.Log4j2;

/**
 * @description 测试无synchronized
 * @author liuc
 * @date 2021/7/22 17:23
 * @since JDK1.8
 * @version V1.0
 */
@Log4j2
public class ThreadNoSynchronized implements Runnable{
    // 一共一百张火车票
    private static int countTicket = 100;
    @Override
    public void run() {
        while (true) {
            if (countTicket > 0) {
                sale();
            } else {
                // 票已经没有了，跳出while true循环
                log.info("票已经卖完！");
                break;
            }
        }
    }

    private synchronized void  sale (){
        if (countTicket > 0) {
            //已经卖出，票的总数减去1
            countTicket --;
            log.info("正在卖票,还剩" + countTicket + "张票");
        }
    }

    public static void main(String[] args) {
        ThreadNoSynchronized r = new ThreadNoSynchronized();
        Thread t1 = new Thread(r,"窗口1");
        Thread t2 = new Thread(r,"窗口2");
        Thread t3 = new Thread(r,"窗口3");
        Thread t4 = new Thread(r,"窗口4");
        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}
