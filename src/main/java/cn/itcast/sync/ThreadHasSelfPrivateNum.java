package cn.itcast.sync;

import lombok.extern.log4j.Log4j2;

/**
 * @description 测试方法内的变量为线程安全的
 * @author liuc
 * @date 2021/7/23 15:23
 * @since JDK1.8
 * @version V1.0
 */
@Log4j2
public class ThreadHasSelfPrivateNum implements Runnable{
    @Override
    public void run() {
        add(Thread.currentThread().getName());
    }

    public void add(String name){
        int i = 0;
        if ("A".equals(name)) {
            i = 100;
        } else {
            i=200;
        }
        log.info(Thread.currentThread().getName()+"线程,i设值完成，i="+i);
    }

    public static void main(String[] args) {
        ThreadHasSelfPrivateNum r = new ThreadHasSelfPrivateNum();
        Thread t = new Thread(r,"A");
        Thread t1= new Thread(r,"B");
        t.start();
        t1.start();
    }
}
