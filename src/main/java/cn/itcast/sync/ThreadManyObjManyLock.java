package cn.itcast.sync;

import lombok.extern.log4j.Log4j2;
import java.util.concurrent.TimeUnit;

/**
 * @description 测试多个对象多个锁问题
 * @author liuc
 * @date 2021/7/26 9:23
 * @since JDK1.8
 * @version V1.0
 */
@Log4j2
public class ThreadManyObjManyLock implements Runnable{
    @Override
    public void run() {
        add(Thread.currentThread().getName());
    }

    public synchronized void add(String name) {
        int i = 0;
        if ("A".equals(name)) {
            try {
                log.info("A 设值完成");
                i = 100;
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else if("B".equals(name)){
            log.info("B 设值完成");
            i = 200;
        }
        log.info(Thread.currentThread().getName() + "线程,i设值完成，i=" + i);
    }

    public static void main(String[] args) {
        ThreadManyObjManyLock r = new ThreadManyObjManyLock();
        ThreadManyObjManyLock r1 = new ThreadManyObjManyLock();
        Thread t = new Thread(r,"A");
        Thread t1= new Thread(r1,"B");
        t.start();
        t1.start();
    }
}
